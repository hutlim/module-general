<?php
/**
 * @uses SearchContext
 * 
 * @package general
 * @subpackage backend
 */
class GeneralModelAdmin extends ModelAdmin {
	private static $menu_title = 'General Model Admin';
	
	public function init() {
		parent::init();

		Requirements::block(FRAMEWORK_ADMIN_DIR . '/javascript/ModelAdmin.js');
        Requirements::javascript('general/javascript/GeneralModelAdmin.min.js');
	}
	
	public function getEditForm($id = null, $fields = null) {
		$list = $this->getList();
		if(ClassInfo::exists('GridFieldExportToExcelButton')){
        	$exportButton = new GridFieldExportToExcelButton('buttons-after-left');
		}
		else{
			$exportButton = new GridFieldExportButton('buttons-after-left');
		}
		$exportButton->setExportColumns($this->getExportFields());
		$listField = GridField::create(
			$this->sanitiseClassName($this->modelClass),
			false,
			$list,
			$fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
				->removeComponentsByType('GridFieldFilterHeader')
				->addComponents(new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
		);

		// Validation
		if(singleton($this->modelClass)->hasMethod('getCMSValidator')) {
			$detailValidator = singleton($this->modelClass)->getCMSValidator();
			$listField->getConfig()->getComponentByType('GridFieldDetailForm')->setValidator($detailValidator);
		}

		$form = CMSForm::create( 
			$this,
			'EditForm',
			new FieldList($listField),
			new FieldList()
		)->setHTMLID('Form_EditForm');
		$form->setResponseNegotiator($this->getResponseNegotiator());
		$form->addExtraClass('cms-edit-form cms-panel-padded center');
		$form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));
		$editFormAction = Controller::join_links($this->Link($this->sanitiseClassName($this->modelClass)), 'EditForm');
		$form->setFormAction($editFormAction);
		$form->setAttribute('data-pjax-fragment', 'CurrentForm');

		$this->extend('updateEditForm', $form);
		
		return $form;
	}

	public function getManagedModels() {
		$models = $this->stat('managed_models');
		if(is_string($models)) {
			$models = array($models);
		}
		if(!count($models)) {
			user_error(
				'ModelAdmin::getManagedModels(): 
				You need to specify at least one DataObject subclass in public static $managed_models.
				Make sure that this property is defined, and that its visibility is set to "public"', 
				E_USER_ERROR
			);
		}

		// Normalize models to have their model class in array key
		foreach($models as $k => $v) {
			if(is_numeric($k)) {
				if(!singleton($v)->canView() && Member::currentUserID()){
					unset($models[$k]);
					continue;
				}
				$models[$v] = array('title' => singleton($v)->i18n_singular_name());
				unset($models[$k]);
			}
			else{
				if(!singleton($k)->canView() && Member::currentUserID()){
					unset($models[$k]);
					continue;
				}
			}
		}
		
		return $models;
	}
}