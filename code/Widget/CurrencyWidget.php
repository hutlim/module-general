<?php

class CurrencyWidget extends Widget {
	private static $title = null; //don't show a title for this widget by default
	private static $cmsTitle = "Currency Widget";
	private static $description = "Show currency rate";
	
	function CurrencyList(){
		return CurrencyRate::get()->limit(10);
	}
}