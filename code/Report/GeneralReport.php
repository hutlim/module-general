<?php
/**
 * @package general
 */
class GeneralReport extends SS_Report implements PermissionProvider {
	/**
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		if(!$member && $member !== FALSE) {
			$member = Member::currentUser();
		}
		
		$extended = $this->extendedCan(__FUNCTION__, $member);
		if($extended !== null) return $extended;

        return Permission::check('VIEW_' . $this->class);
	}
	
	public function extendedCan($methodName, $member) {
		$results = $this->extend($methodName, $member);
		if($results && is_array($results)) {
			// Remove NULLs
			$results = array_filter($results, function($v) {return !is_null($v);});
			// If there are any non-NULL responses, then return the lowest one of them.
			// If any explicitly deny the permission, then we don't get access 
			if($results) return min($results);
		}
		return null;
	}
	
	public function getReportField() {
		$gridfield = parent::getReportField();
		
		$columns = array();
		foreach($this->columns() as $source => $info) {
			if(is_string($info)) $info = array('title' => $info);
			$columns[$source] = isset($info['title']) ? $info['title'] : $source;
		}

		$gridfield->setTitle($this->title());
		if(ClassInfo::exists('GridFieldExportToExcelButton')){
			$gridfield->getConfig()->removeComponentsByType('GridFieldExportButton')->addComponent(new GridFieldExportToExcelButton('buttons-after-left'));
			$gridfield->getConfig()->getComponentByType('GridFieldExportToExcelButton')->setExportColumns($columns);
		}
		else{
			$gridfield->getConfig()->getComponentByType('GridFieldExportButton')->setExportColumns($columns);
		}
		
		$gridfield->getConfig()->getComponentByType('GridFieldPrintButton')->setPrintColumns($columns);
		
		return $gridfield;
	}
	
	public function providePermissions() {
		foreach(ClassInfo::subclassesFor('GeneralReport') as $i => $class) {
			if($class == 'GeneralReport') continue;
			if(ClassInfo::classImplements($class, 'TestOnly')) continue;

			$obj = Injector::inst()->create($class);
			$title = $obj->title() ? $obj->title() : $class;
			$perms["VIEW_" . $class] = array(
				'name' => _t('GeneralReport.PERMISSION_VIEW', 'Allow view access right ({title})', '', array('title' => $title)),
                'category' => _t('GeneralReport.PERMISSIONS_CATEGORY', 'Report')
			);
		}

		return $perms;
	}
}