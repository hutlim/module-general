<?php

class DashboardPanelAjaxAction extends DashboardPanelAction {
	/**
	 * Gets the HTML link
	 *
	 * @return string
	 */
	public function forTemplate() {
		return "<a href='$this->Link' class='dashboard-panel-action cms-panel-link ss-ui-button {$this->getUIClass()}'>$this->Title</a>";
	}
}