<?php

/**
 * @package general
 */
class AdminProfileController extends CMSProfileController {
	private static $url_priority = 100;
	
	public function handleRequest(SS_HTTPRequest $request, DataModel $model = null) {
		$extensions = Member::get_extensions('Member');
		foreach($extensions as $extension){
			if(in_array($extension, (array)Config::inst()->get('UserAdmin', 'user_extensions'))) continue;
			Member::remove_extension($extension);
		}
		
		return parent::handleRequest($request, $model);
	}
	
	public function getEditForm($id = null, $fields = null) {
		$form = parent::getEditForm($id, $fields);
		
		if($form instanceof SS_HTTPResponse) {
			return $form;
		}
		
		if(Member::currentUser()->Email == Security::default_admin_username()){
			$form->Fields()->removeByName('Password');
			$form->Fields()->makeFieldReadonly('Email');
			$form->Fields()->dataFieldByName('Email')->setIncludeHiddenField(true);
		}
		
		$form->Fields()->removeByName('DateFormat');
		$form->Fields()->removeByName('TimeFormat');
		$form->Fields()->removeByName('Permissions');
		$form->Fields()->makeFieldReadonly('Status');
		$form->Fields()->makeFieldReadonly('DirectGroups');
		
		return $form;
	}
	
	public function canView($member = null) {
		if(!$member && $member !== FALSE) $member = Member::currentUser();

		// cms menus only for logged-in members
		if(!$member) return false;
		
		// Only check for generic admin permissions
		if(
			!$member->IsAdmin
		) {
			return false;
		}

		return true;
	}
}
