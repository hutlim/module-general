<?php

class AdminLogin extends Security {
    private static $allowed_actions = array(
    	'login',
        'LoginForm',
        'lostpassword',
        'LostPasswordForm',
        'passwordsent'
    );
	
	/**
	 * Show the "login" page
	 *
	 * @return string Returns the "login" page as HTML code.
	 */
	public function login() {
		$controller = Controller::curr();
		if($controller instanceof Dashboard && $controller instanceof DatabaseAdmin && $controller instanceof DevelopmentAdmin && $controller instanceof LeftAndMain && !preg_match('/\/admin/i', $this->request->getVar('BackURL')) && !preg_match('/\/dev/i', $this->request->getVar('BackURL'))){
			return $this->redirect('member/login');
		}
		
		if(preg_match('/\/member-area/i', $this->request->getVar('BackURL'))){
			return $this->redirect('member/login');
		}
		
		Requirements::themedCSS('AdminLogin');

		// Get response handler
		$controller = $this->getResponseController(_t('AdminLogin.LOGIN', 'Administrator Log In'));

		// if the controller calls Director::redirect(), this will break early
		if(($response = $controller->getResponse()) && $response->isFinished()) return $response;

		$form = $this->LoginForm();
		$content = $form->forTemplate();
		
		if($message = $this->getLoginMessage()) {
			$customisedController = $controller->customise(array(
				"Content" => $message,
				"Form" => $content,
			));
		} else {
			$customisedController = $controller->customise(array(
				"Form" => $content,
			));
		}
		
		Session::clear('Security.Message');

		// custom processing
		return $customisedController->renderWith($this->getTemplatesFor('admin_login'));
	}

	/**
	 * Get a link to a security action
	 *
	 * @param string $action Name of the action
	 * @return string Returns the link to the given action
	 */
	public function Link($action = null) {
		return Controller::join_links(Director::baseURL(), "administrator", $action);
	}
    
    function LoginForm(){
        return AdminAuthenticator::get_login_form($this);
    }
	
	/**
	 * Prepare the controller for handling the response to this request
	 *
	 * @param string $title Title to use
	 * @return Controller
	 */
	protected function getResponseController($title) {
		if(!class_exists('SiteTree')) return $this;

		// Use sitetree pages to render the security page
		$tmpPage = new Page();
		$tmpPage->Title = $title;
		$tmpPage->URLSegment = "administrator";
		// Disable ID-based caching  of the log-in page by making it a random number
		$tmpPage->ID = -1 * rand(1,10000000);

		$controller = Page_Controller::create($tmpPage);
		$controller->setDataModel($this->model);
		$controller->init();
		return $controller;
	}
	
	/**
	 * Show the "lost password" page
	 *
	 * @return string Returns the "lost password" page as HTML code.
	 */
	public function lostpassword() {
		$controller = $this->getResponseController(_t('AdminLogin.LOSTPASSWORDHEADER', 'Reset Login Password'));

		// if the controller calls Director::redirect(), this will break early
		if(($response = $controller->getResponse()) && $response->isFinished()) return $response;

		$customisedController = $controller->customise(array(
			'Content' => 
				'<p>' . 
				_t(
					'AdminLogin.NOTERESETPASSWORD', 
					'We will send you a link with which you can reset your password'
				) . 
				'</p>',
			'Form' => $this->LostPasswordForm(),
		));
		
		//Controller::$currentController = $controller;
		return $customisedController->renderWith($this->getTemplatesFor('lostpassword'));
	}
	
	/**
	 * Factory method for the lost password form
	 *
	 * @return Form Returns the lost password form
	 */
	public function LostPasswordForm() {
		$label=singleton('Member')->fieldLabel(Member::config()->unique_identifier_field);
		return AdminLoginForm::create($this,
			'LostPasswordForm',
			new FieldList(
				new TextField('Email', $label)
			),
			new FieldList(
				new FormAction(
					'forgotPassword',
					_t('AdminLogin.BUTTONSEND', 'Reset Password')
				)
			),
			false,
			false
		);
	}
	
	/**
	 * Show the "password sent" page, after a user has requested
	 * to reset their password.
	 *
	 * @param SS_HTTPRequest $request The SS_HTTPRequest for this action. 
	 * @return string Returns the "password sent" page as HTML code.
	 */
	public function passwordsent($request) {
		$controller = $this->getResponseController(_t('AdminLogin.LOSTPASSWORDHEADER', 'Reset Login Password'));

		// if the controller calls Director::redirect(), this will break early
		if(($response = $controller->getResponse()) && $response->isFinished()) return $response;

		$email = Convert::raw2xml(rawurldecode($request->param('ID')) . '.' . $request->getExtension());

		$customisedController = $controller->customise(array(
			'Title' => _t('AdminLogin.PASSWORDSENTHEADER', "Password reset link sent to email address",
				array('email' => $email)),
			'Content' =>
				"<p>"
				. _t('AdminLogin.PASSWORDSENTTEXT', 
					"Thank you! A reset link has been sent to your email address, provided an account exists for this email address.")
				. "</p>",
			'Email' => $email
		));
		
		//Controller::$currentController = $controller;
		return $customisedController->renderWith($this->getTemplatesFor('passwordsent'));
	}
}
