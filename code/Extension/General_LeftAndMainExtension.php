<?php

class General_LeftAndMainExtension extends LeftAndMainExtension {
    function init(){
    	Requirements::add_i18n_javascript('general/javascript/lang');
		CMSMenu::remove_menu_item('Help');
    }

	function onBeforeInit(){
		if(!$this->owner->canView()) {
			// When access /admin/, we should try a redirect to another part of the admin rather than be locked out
			$menu = $this->owner->MainMenu();
			foreach($menu as $candidate) {
				if(
					$candidate->Link &&
					$candidate->Link != $this->owner->Link()
					&& $candidate->MenuItem->controller
					&& singleton($candidate->MenuItem->controller)->canView()
				) {
					return $this->owner->redirect($candidate->Link);
				}
			}

			if(Member::currentUser()) {
				Session::set("BackURL", null);
			}

			$messageSet = array(
				'default' => _t(
					'General_LeftAndMainExtension.PERMDEFAULT',
					"You must be logged in to access the admin backend; please enter your credentials below."
				),
				'alreadyLoggedIn' => _t(
					'General_LeftAndMainExtension.PERMALREADY',
					"I'm sorry, but you can't access that part of the admin backend.  If you want to log in as someone else, do"
					. " so below."
				),
				'logInAgain' => _t(
					'General_LeftAndMainExtension.PERMAGAIN',
					"You have been logged out of the admin backend.  If you would like to log in again, enter a username and"
					. " password below."
				),
			);

			$member = Member::currentUser();
			$response = Security::permissionFailure($this->owner, $messageSet);
			if($response && !Director::is_ajax()){
				$member = Member::currentUser();
	
				// Work out the right message to show
				if($member && $member->exists()) {
					if(isset($messageSet['alreadyLoggedIn'])) {
						$message = $messageSet['alreadyLoggedIn'];
					} else {
						$message = $messageSet['default'];
					}
	
					$me = new Security();
					$form = $me->LoginForm();
					$form->sessionMessage($message, 'warning');
					Session::set('MemberLoginForm.force_message',1);
					
					$response->redirect('/administrator/login');
				}
			}
		}
	}
}

?>
