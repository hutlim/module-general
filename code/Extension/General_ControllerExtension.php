<?php

class General_ControllerExtension extends Extension {
    private static $allowed_actions = array('AdminLoginForm');
    
    function onBeforeInit(){
        if($this->owner->request->requestVar('locale')){
            Cookie::set('locale', $this->owner->request->requestVar('locale'));
        } else if($this->owner->request->requestVar('Locale')){
            Cookie::set('locale', $this->owner->request->requestVar('Locale'));
        } else if($this->owner->dataRecord && $this->owner->dataRecord->hasExtension('Translatable') && $this->owner->dataRecord->exists()) {
            Cookie::set('locale', $this->owner->dataRecord->Locale);
        }

        if($locale = Cookie::get('locale')){
			if(is_string($locale)){
				$locale = preg_replace('/[^a-zA-Z-_]/', '', $locale);
				$split = explode('_', $locale);
				if(!empty($split[0]) && !empty($split[1]) && is_string($split[0]) && is_string($split[1])){
					$locale = sprintf('%s_%s', strtolower($split[0]), strtoupper($split[1]));
				}
            
				if(i18n::validate_locale($locale)){
					i18n::set_locale($locale);
					if(ClassInfo::exists('Translatable')){
						$_REQUEST['locale'] = $locale;
						Translatable::set_current_locale($locale);
					}
				}
				else{
					Cookie::set('locale', '');
					$_REQUEST['locale'] = '';
				}
			}
			else{
				Cookie::set('locale', '');
				$_REQUEST['locale'] = '';
			}
        }
        
        if($this->owner instanceof MemberAreaPage_Controller && !Distributor::customLoginID() && $this->owner->CurrentMember() && $this->owner->CurrentMember()->IsDistributor && $this->owner->CurrentMember()->Session != session_id()){
            $this->owner->CurrentMember()->logOut();        
        }
    }
    
    function AdminLoginForm(){
        return AdminLoginForm::create($this->owner, "AdminLoginForm");
    }
}

?>
