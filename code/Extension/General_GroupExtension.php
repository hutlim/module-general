<?php

class General_GroupExtension extends DataExtension implements PermissionProvider {
    private static $db = array('IsAdminGroup' => 'Boolean');

	function requireDefaultRecords() {
        if(!$staffGroup = Group::get()->find('Code', 'admin')) {
            Group::create()
            ->setField('Code', 'admin')
            ->setField('IsAdminGroup', 1)
            ->setField('Title', 'Admin')
            ->write();
            DB::alteration_message('Admin group created', 'created');
        }
    }
	
    function onBeforeWrite(){
        if($this->owner->Code == 'administrators' && !$this->owner->ID){
            $this->owner->IsAdminGroup = 1;
        }
    }

    function updateCMSFields(FieldList $fields) {
    	$fields->insertAfter(TextField::create('Code', $this->owner->fieldLabel('Code')), 'Title');
		$fields->insertAfter(NumericField::create('Sort', $this->owner->fieldLabel('Sort')), 'Code');
        $fields->insertAfter(CheckboxField::create('IsAdminGroup', _t('General_GroupExtension.IS_ADMIN_GROUP', 'This is the admin group.')), 'ParentID');
    }
	
	function canCreate($member){
		return Permission::checkMember($member, 'EDIT_GROUPS');
	}
	
	public function providePermissions() {
		return array(
			'EDIT_GROUPS' => array(
				'name' => _t('General_GroupExtension.EDIT_GROUPS', 'Manage groups'),
				'category' => _t('Permissions.PERMISSIONS_CATEGORY')
			)
		);
	}
}
