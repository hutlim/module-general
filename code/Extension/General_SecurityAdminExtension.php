<?php

class General_SecurityAdminExtension extends LeftAndMainExtension {
	function updateEditForm($form){
		if($form->Fields()->dataFieldByName('Members')){
			$form->Fields()->dataFieldByName('Members')->getConfig()->addComponent(new MemberGridFieldStatusAction());
		}
		
		if($form->Fields()->dataFieldByName('Groups')){
			$form->Fields()->dataFieldByName('Groups')->getConfig()->addComponent(new GridFieldOrderableRows());
		}
	}
}
