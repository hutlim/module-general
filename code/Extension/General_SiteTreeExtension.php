<?php

class General_SiteTreeExtension extends DataExtension {
    public function contentcontrollerInit($controller) {
    	LiveVersioned::choose_site_stage();
		
		$currency = SiteCurrencyConfig::current_site_currency();
        if($currency){
            Config::inst()->update('Currency', 'currency_symbol', $currency);
        }
			
        $files = array();

        // jquery
        $files[] = 'general/thirdparty/jquery/jquery.min.js';
		$files[] = 'general/thirdparty/jquery/jquery-migrate.min.js';
		Requirements::block(THIRDPARTY_DIR . '/jquery/jquery.js');
        Requirements::block(THIRDPARTY_DIR . '/jquery/jquery.min.js');

        // jquery on demand
        $files[] = FRAMEWORK_DIR . '/javascript/jquery-ondemand/jquery.ondemand.js';
		
		// jquery ui
		$files[] = FRAMEWORK_DIR . '/thirdparty/jquery-ui/jquery-ui.js';
		
		$files[] = FRAMEWORK_DIR . '/javascript/i18n.js';
		Requirements::add_i18n_javascript('general/javascript/lang');

        // bootstrap
        $files[] = 'general/thirdparty/bootstrap/js/bootstrap.min.js';
        Requirements::css('general/thirdparty/bootstrap/css/bootstrap.min.css');
		
		// jquery input mask
        $files[] = 'general/thirdparty/jquery-inputmask/dist/min/jquery.inputmask.bundle.min.js';

        // ajax submit form
        Requirements::block(THIRDPARTY_DIR . '/jquery-form/jquery.form.js');
        Requirements::block(THIRDPARTY_DIR . '/jquery-validate/lib/jquery.form.js');
		$files[] = 'general/thirdparty/jquery-form/jquery.form.min.js';
		
		// jquery detect mobile
        $files[] = 'general/thirdparty/jquery-detectmobile/detectmobilebrowser.js';

        // i18n
        $files[] = FRAMEWORK_DIR . '/javascript/i18n.js';

        // general js config
        if(Director::isDev()){
            $files[] = 'general/javascript/general.js';
        }
        else{
            $files[] = 'general/javascript/general.min.js';
        }

        Requirements::combine_files('general_core.js', $files);
		
		$date = _t('General.CLOCK_DATE', '"", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"');
		$day = _t('General.CLOCK_DAY', '"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"');
		$month = _t('General.CLOCK_MONTH', '"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"');
		$year = _t('General.CLOCK_YEAR', '');
		$server_timezone = new DateTimeZone(date_default_timezone_get());
		$server_time = new DateTime('now', $server_timezone);
		$timezone_offset = "GMT" . $server_time->format('P');
		$time = $server_time->getTimestamp();
		$js = <<<JS
				(function($) {
					$(document).ready(function() {
			            var timer = $time*1000;
			            setInterval(function(){clock();}, 1000);
						
						function clock(){
							var gd = new Date(timer);
							var secs = gd.getSeconds();
							var minutes = gd.getMinutes();
							var hours = gd.getHours();
							var day = gd.getDay();
							var month = gd.getMonth();
							var date = gd.getDate();
							var year = gd.getYear();
							var m = hours >= 12 ? 'PM' : 'AM';
							
							year = year < 1000 ? year + 1900 : year;
							hours = hours % 12;
  							hours = hours ? hours : 12;
							minutes = minutes < 10 ? '0'+minutes : minutes;
							secs = secs < 10 ? '0'+secs : secs;
						
							var datearray = new Array ($date);
							var dayarray = new Array ($day);
							var montharray = new Array ($month);
						
							var fulldate = "<p>" +dayarray[day]+", "+montharray[month]+" "+datearray[date]+", "+year+"$year</p><p>"+hours+":"+minutes+":"+secs+" "+m+ " ($timezone_offset)</p>";
							$('#showclock').html(fulldate);
	
							timer = timer + 1000;
						}
					});
				})(jQuery);
JS;

		Requirements::customScript($js, 'ShowClock');

        HtmlEditorConfig::set_active('basic');
        $htmlEditorConfig = HtmlEditorConfig::get('basic');
        $htmlEditorConfig->setOptions(array(
            "friendly_name" => "Basic Editor",
            "priority" => 60,
            "body_class" => "typography",
            "mode" => "textareas",
            "editor_selector" => "htmleditor",
            "auto_resize" => true,
            "verify_html" => true,
            "theme" => "advanced",
            "language" => i18n::get_tinymce_lang(),
            "browser_spellcheck" => true,
            "relative_urls" => true,
            "safari_warning" => false,
            "table_inline_editing" => true,
            "blockquote_clear_tag" => "p",
            "document_base_url" => isset($_SERVER['HTTP_HOST']) ? Director::absoluteBaseURL() : null,
            "use_native_selects" => false,
            "theme_advanced_statusbar_location" => "none",
            "theme_advanced_layout_manager" => "SimpleLayout",
            "theme_advanced_toolbar_location" => "top",
            "theme_advanced_toolbar_align" => "left",
            "valid_elements" => "@[id|class|style|title],a[id|rel|rev|dir|tabindex|accesskey|type|name|href|target|title" . "|class],-strong/-b[class],-em/-i[class],-strike[class],-u[class],#p[id|dir|class|align|style],-ol[class]," . "-ul[class],-li[class],br,img[id|dir|longdesc|usemap|class|src|border|alt=|title|width|height|align|data*]," . "-sub[class],-sup[class],-blockquote[dir|class],-cite[dir|class|id|title]," . "-table[border=0|cellspacing|cellpadding|width|height|class|align|summary|dir|id|style]," . "-tr[id|dir|class|rowspan|width|height|align|valign|bgcolor|background|bordercolor|style]," . "tbody[id|class|style],thead[id|class|style],tfoot[id|class|style]," . "#td[id|dir|class|colspan|rowspan|width|height|align|valign|scope|style]," . "-th[id|dir|class|colspan|rowspan|width|height|align|valign|scope|style],caption[id|dir|class]," . "-div[id|dir|class|align|style],-span[class|align|style],-pre[class|align],address[class|align]," . "-h1[id|dir|class|align|style],-h2[id|dir|class|align|style],-h3[id|dir|class|align|style]," . "-h4[id|dir|class|align|style],-h5[id|dir|class|align|style],-h6[id|dir|class|align|style],hr[class]," . "dd[id|class|title|dir],dl[id|class|title|dir],dt[id|class|title|dir],@[id,style,class]",
            'extended_valid_elements' => "img[class|src|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name" . "|usemap|data*],iframe[src|name|width|height|align|frameborder|marginwidth|marginheight|scrolling]," . "object[width|height|data|type],param[name|value],map[class|name|id],area[shape|coords|href|target|alt]"
        ));
        $htmlEditorConfig->enablePlugins('inlinepopups,advimage,autolink,advlink,youtube');
        // Add the buttons you would like to add, see http://www.tinymce.com/wiki.php/buttons/controls for a comprehensive list
        $htmlEditorConfig->setButtonsForLine(1, "bold", "italic", "underline", "strikethrough", "separator", "justifyleft", "justifycenter", "justifyright", "justifyfull", "separator", "bullist", "numlist", "outdent", "indent");
        $htmlEditorConfig->setButtonsForLine(2, "tablecontrols");
        $htmlEditorConfig->setButtonsForLine(3, "undo", "redo", "separator", "link", "unlink", "image", "youtube", "separator", "forecolor", "backcolor");
        $htmlEditorConfig->setButtonsForLine(4, "formatselect", "fontselect", "fontsizeselect");
    }

    public function modelascontrollerInit($controller) {
        LiveVersioned::choose_site_stage();
    }

    public function setMessage($type, $message) {
        if($type == 'error'){
            $type = 'danger';
        }
        Session::set('Message', array(
            'MessageType' => strtolower($type),
            'Message' => $message
        ));
    }

    public function getMessage() {
        if($message = Session::get('Message')) {
            Session::clear('Message');
            $array = new ArrayData($message);
            return $array->renderWith('Message');
        }
    }

}
?>
