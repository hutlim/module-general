<?php
  
class General_RequestFilter implements RequestFilter {

    public function preRequest(SS_HTTPRequest $request, Session $session, DataModel $model) {
		$session_id = isset($_COOKIE[session_name()]) ? $_COOKIE[session_name()] : '';
        if(!empty($session_id) && ($session_id !== (string)$session_id || !preg_match('/^[0-9a-zA-Z,-]{22,40}$/', $_COOKIE[session_name()]))) {
            Session::destroy();  
        }

        $locale = $request->requestVar('locale');
        if(!empty($locale) && (!is_string($locale) || !i18n::validate_locale($locale))){
            $this->redirect();      
        }
        
        $locale = $request->requestVar('Locale');
        if(!empty($locale) && (!is_string($locale) || !i18n::validate_locale($locale))){
            $this->redirect();      
        }
        
        $back_url = $request->requestVar('BackURL');
        if(!empty($back_url) && !is_string($back_url)){
            $this->redirect();
        }
        
        return true;                    
    }

    public function postRequest(SS_HTTPRequest $request, SS_HTTPResponse $response, DataModel $model) {
        return true;
    }
    
    public function redirect(){
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('?', $actual_link);
        header("Location: " . $url[0], true, 301);
        die();    
    }
}