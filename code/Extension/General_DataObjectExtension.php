<?php

class General_DataObjectExtension extends DataExtension {
    function augmentSQL(SQLQuery &$query, DataQuery &$dataQuery = null) {
        if($this->owner->hasDatabaseField('ID')){
            $baseClass = ClassInfo::baseDataClass($this->owner->class);
            $query->addWhere(sprintf('"%s"."%s" > 0', $baseClass, 'ID'));
        }
        
		if(!in_array($this->owner->ClassName, array('AuditTrail', 'AuditTrailDetail'))){
			if($this->owner->hasDatabaseField('MemberID')){
				$baseClass = ClassInfo::baseDataClass($this->owner->class);
				$query->addWhere(sprintf('"%s"."%s" > 0', $baseClass, 'MemberID'));  
			}
		}
    }
}
