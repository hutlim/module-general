<?php

class SiteCurrencyConfig extends DataExtension {
    private static $default_currency = 'USD';

    private static $db = array('SiteCurrency' => 'Varchar(3)');

    function populateDefaults() {
    	require_once 'Zend/Currency.php';
        if(!$this->owner->Locale || !i18n::validate_locale($this->owner->Locale)){
            $this->owner->Locale = i18n::get_locale();    
        }
		$currency = new Zend_Currency($this->owner->Locale);
		$code = $currency->getShortName();
		
		if(!$code){
			$code = Config::inst()->get($this->owner->class, 'default_currency', Config::FIRST_SET);
		}
		
        $this->owner->SiteCurrency = $code;
    }

    function updateCMSFields(FieldList $fields) {
        $fields->addFieldToTab("Root.Main", CurrencyDropdownField::create('SiteCurrency', _t('SiteCurrencyConfig.SITE_CURRENCY', 'Site Currency')));
    }

    static function current_site_currency() {
    	$code = '';
        if(DB::isActive() && ClassInfo::hasTable('SiteConfig') && !DB::getConn()->isSchemaUpdating()){
            $code = SiteConfig::current_site_config()->SiteCurrency;
        }
		
		if(!$code){
			require_once 'Zend/Currency.php';
			$currency = new Zend_Currency(i18n::get_locale());
			$code = $currency->getShortName();
		}
		
		if(!$code){
        	Config::inst()->get('SiteCurrencyConfig', 'default_currency');
		}
		
		return $code;
    }

}
