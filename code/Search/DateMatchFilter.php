<?php
/**
 * @package framework
 * @subpackage search
 */

/**
 * Matches textual content with a LIKE '%keyword%' construct.
 *
 * @package framework
 * @subpackage search
 */
class DateMatchFilter extends SearchFilter {
	protected function applyOne(DataQuery $query) {
		require_once 'Zend/Date.php';
		$this->model = $query->applyRelation($this->relation);
		$modifiers = $this->getModifiers();
		$valueObj = new Zend_Date($this->getValue(), 'dd/MM/yyyy', i18n::get_locale());
		$where = DB::getConn()->comparisonClause(
			sprintf("DATE(%s)", $this->getDbName()),
			$valueObj->get('yyyy-MM-dd'),
			true, // exact?
			false, // negate?
			$this->getCaseSensitive()
		);

		return $query->where($where);
	}

	protected function applyMany(DataQuery $query) {
		require_once 'Zend/Date.php';
		$this->model = $query->applyRelation($this->relation);
		$where = array();
		$modifiers = $this->getModifiers();
		$valueObj = new Zend_Date($this->getValue(), 'dd/MM/yyyy', i18n::get_locale());
		foreach($this->getValue() as $value) {
			$where[]= DB::getConn()->comparisonClause(
				sprintf("DATE(%s)", $this->getDbName()),
				$valueObj->get('yyyy-MM-dd'),
				true, // exact?
				false, // negate?
				$this->getCaseSensitive()
			);
		}

		return $query->where(implode(' OR ', $where));
	}

	protected function excludeOne(DataQuery $query) {
		require_once 'Zend/Date.php';
		$this->model = $query->applyRelation($this->relation);
		$modifiers = $this->getModifiers();
		$valueObj = new Zend_Date($this->getValue(), 'dd/MM/yyyy', i18n::get_locale());
		$where = DB::getConn()->comparisonClause(
			sprintf("DATE(%s)", $this->getDbName()),
			$valueObj->get('yyyy-MM-dd'),
			true, // exact?
			true, // negate?
			$this->getCaseSensitive()
		);
		
		return $query->where($where);
	}

	protected function excludeMany(DataQuery $query) {
		require_once 'Zend/Date.php';
		$this->model = $query->applyRelation($this->relation);
		$where = array();
		$modifiers = $this->getModifiers();
		$valueObj = new Zend_Date($this->getValue(), 'dd/MM/yyyy', i18n::get_locale());
		foreach($this->getValue() as $value) {
			$where[]= DB::getConn()->comparisonClause(
				sprintf("DATE(%s)", $this->getDbName()),
				$valueObj->get('yyyy-MM-dd'),
				true, // exact?
				true, // negate?
				$this->getCaseSensitive()
			);
		}

		return $query->where(implode(' AND ', $where));
	}
	
	public function isEmpty() {
		return $this->getValue() === array() || $this->getValue() === null || $this->getValue() === '';
	}
}
