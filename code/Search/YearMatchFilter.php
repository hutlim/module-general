<?php
/**
 * @package framework
 * @subpackage search
 */

/**
 * Matches textual content with a LIKE '%keyword%' construct.
 *
 * @package framework
 * @subpackage search
 */
class YearMatchFilter extends SearchFilter {
	protected function applyOne(DataQuery $query) {
		$this->model = $query->applyRelation($this->relation);
		$modifiers = $this->getModifiers();
		$where = DB::getConn()->comparisonClause(
			sprintf("DATE_FORMAT(%s, '%%Y')", $this->getDbName()),
			Convert::raw2sql($this->getValue()),
			true, // exact?
			false, // negate?
			$this->getCaseSensitive()
		);

		return $query->where($where);
	}

	protected function applyMany(DataQuery $query) {
		$this->model = $query->applyRelation($this->relation);
		$where = array();
		$modifiers = $this->getModifiers();
		foreach($this->getValue() as $value) {
			$where[]= DB::getConn()->comparisonClause(
				sprintf('DATE_FORMAT(%s, "%Y")', $this->getDbName()),
				Convert::raw2sql($this->getValue()),
				true, // exact?
				false, // negate?
				$this->getCaseSensitive()
			);
		}

		return $query->where(implode(' OR ', $where));
	}

	protected function excludeOne(DataQuery $query) {
		$this->model = $query->applyRelation($this->relation);
		$modifiers = $this->getModifiers();
		$where = DB::getConn()->comparisonClause(
			sprintf('DATE_FORMAT(%s, "%Y")', $this->getDbName()),
			Convert::raw2sql($this->getValue()),
			true, // exact?
			true, // negate?
			$this->getCaseSensitive()
		);
		
		return $query->where($where);
	}

	protected function excludeMany(DataQuery $query) {
		$this->model = $query->applyRelation($this->relation);
		$where = array();
		$modifiers = $this->getModifiers();
		foreach($this->getValue() as $value) {
			$where[]= DB::getConn()->comparisonClause(
				sprintf('DATE_FORMAT(%s, "%%%Y")', $this->getDbName()),
				Convert::raw2sql($this->getValue()),
				true, // exact?
				true, // negate?
				$this->getCaseSensitive()
			);
		}

		return $query->where(implode(' AND ', $where));
	}
	
	public function isEmpty() {
		return $this->getValue() === array() || $this->getValue() === null || $this->getValue() === '';
	}
}
