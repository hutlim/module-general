<?php
class UsernameField extends AutoCompleteField {
    function __construct($name, $title = null, $value = '', $maxLength = null, $form = null) {
        parent::__construct($name, $title, $value, $maxLength, $form, 'Member', 'Username');
    }
	
	function Type() {
		return parent::Type() . ' username';
	}
    
    function validate($validator) {
        if(!Member::get()->filter('Username', $this->dataValue())->count()) {
            $validator->validationError($this->name, _t('UsernameField.USERNAME_INVALID', "The username is invalid"));
            return false;
        }

        return true;
    }
}
?>
