<?php
class CurrencyDropdownField extends DropdownField {
	private static $allowed_actions = array(
        'get_currency_list'
    );
	
	/**
     * The url to use as the check bank list
     * @var string
     */
    protected $currencyListURL;
	
	protected $countryField;
	
	protected $country;
	
	function __construct($name, $title = null, $value = '', $form = null) {
		Requirements::javascript(THIRDPARTY_DIR . '/jquery/jquery.js');
		Requirements::javascript(FRAMEWORK_DIR . '/javascript/i18n.js');
		Requirements::add_i18n_javascript('general/javascript/lang');
        Requirements::javascript('general/javascript/CurrencyDropdownField.min.js');
		Requirements::css('general/css/CurrencyDropdownField.css');
		parent::__construct($name, $title, array(), $value, $form);
	}
	
	function Field($properties = array()) {
		if($this->getCountryField() && !$this->getCountry()){
			$this->setDisabled(true);
		}
		
		$source = $this->getSource();
		if(!$this->Value() || !isset($source[$this->Value()])) {
			$this->setValue(SiteCurrencyConfig::current_site_currency());
		}

        return parent::Field($properties);
    }
	
	function getAttributes() {
        return array_merge(
            parent::getAttributes(), array(
            	'rel' => 'currency',
                'data-url' => $this->getCurrencyListURL(),
                'data-country-field' => $this->getCountryField()
            )
        );
    }
	
	function Type() {
		return 'currency dropdown';
	}
	
	function setSource($source){
		return $this;
	}
	
	function getSource(){
		$source = array();
		
		if($this->getCountryField()){
			$country = $this->getCountry()? $this->getCountry() : $this->form->controller->request->requestVar($this->getCountryField());
			if($country){
				$obj = CurrencyRate::get()->filter('FromCurrency', SiteCurrencyConfig::current_site_currency());
				if($obj->filter('Country', $country)->count()) {
					$source = $obj->filter('Country', $country)->map('ToCurrency', 'ToCurrencyName')->toArray();
				}
				else if($obj->filter('ToCurrency', SiteCurrencyConfig::current_site_currency())->count()) {
					$source = $obj->filter('ToCurrency', SiteCurrencyConfig::current_site_currency())->map('ToCurrency', 'ToCurrencyName')->toArray();
				}
				else {
					require_once 'Zend/Currency.php';
					$currency = new Zend_Currency();
					$data = $currency->getCurrencyList($country);
					foreach($data as $code){
						$this->setValue($code);
						$source[$code] = $currency->getName($code, i18n::get_locale());
					}
				}
			}
		}
		else{
			$source = Zend_Locale::getTranslationList('nametocurrency', i18n::get_locale());
		}
		
		if(class_exists('Collator') && ($collator = Collator::create(i18n::get_locale()))) {
			$collator->asort($source);
		}
		else {
			asort($source);
		}
		
		return $source;
	}

	public function performReadonlyTransformation() {
		$field = $this->castedCopy('ReadonlyCurrencyDropdownField');
		$field->setReadonly(true);
		
		return $field;
	}
	
	/**
     * Set the URL used to get currency list.
     * 
     * @param string $URL The URL used for get currency list.
     */
    function setCurrencyListURL($url) {
        $this->currencyListURL = $url;
    }
	
	/**
     * Get the URL used to get currency list.
     *  
     * @return The URL used for get currency list.
     */
    function getCurrencyListURL() {
        if (!empty($this->currencyListURL)){
            return $this->currencyListURL;
		}

        // Attempt to link back to itself
        return $this->Link('get_currency_list');
    }
	
	/**
     * Set the country field used for get currency list.
     * 
     * @param string $countryField The country field used for get currency list.
     */
    function setCountryField($countryField) {
        $this->countryField = $countryField;
		return $this;
    }
	
	/**
     * Get the country field used to get currency list.
     *  
     * @return string $countryField The country field used for get currency list.
     */
    function getCountryField() {
        return $this->countryField;
    }
	
	/**
     * Set the country used for get currency list.
     * 
     * @param string $country The country used for get currency list.
     */
    function setCountry($country) {
        $this->country = $country;
		return $this;
    }
	
	/**
     * Get the country used to get currency list.
     *  
     * @return string $country The country used for get currency list.
     */
    function getCountry() {
        return $this->country;
    }
	
	/**
     * Handle a request for currency list checking.
     * 
     * @param HTTPRequest $request The request to handle.
     * @return bank list result
     */
    function get_currency_list(HTTPRequest $request) {
    	$this->setCountry($request->getVar('country'));
		return $this->Field();
    }
}


class ReadonlyCurrencyDropdownField extends LookupField {
	function getSource(){
		return Zend_Locale::getTranslationList('nametocurrency', i18n::get_locale());
	}
}
	