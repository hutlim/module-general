<?php

class AdminPermissionCheckboxSetField extends PermissionCheckboxSetField {
	function setSource($source){
		$this->source = $source;
		return $this;	
	}
	
	function getSource(){
		return $this->source;
	}
}