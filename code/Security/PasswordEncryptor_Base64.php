<?php
/**
 * Uses base64 encyrption. Able to decryption.
 * 
 * @package general
 */
class PasswordEncryptor_Base64 extends PasswordEncryptor {
	public function check($hash, $password, $salt = null, $member = null) {
		return strtoupper($password) === strtoupper($this->decrypt($hash, $salt, $member));
	}
	
    public function encrypt($password, $salt = null, $member = null) {
        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($salt), $password, MCRYPT_MODE_CBC, md5(md5($salt))));
    }
	
	public function decrypt($hash, $salt = null, $member = null) {
        return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($salt), base64_decode($hash), MCRYPT_MODE_CBC, md5(md5($salt))), "\0");
    }
}