<?php

class GeneralCurrency extends Currency {
	public function __construct($name = null, $wholeSize = 15, $decimalSize = 2, $defaultValue = 0) {
		parent::__construct($name, $wholeSize, $decimalSize, $defaultValue);
	}
	
	/**
	 * Returns the number as a currency, eg “$1,000.00”.
	 */
	public function Nice() {
		// return "<span title=\"$this->value\">$" . number_format($this->value, 2) . '</span>';
		$val = $this->config()->currency_symbol . number_format(abs($this->value), $this->decimalSize);
		if($this->value < 0) return "($val)";
		else return $val;
	}
    
    public function getWholeSize(){
        return $this->wholeSize;
    }
    
    public function getDecimalSize(){
        return $this->decimalSize;    
    }
}
