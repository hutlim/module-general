<?php
Class CountryField extends CountryDropdownField {
	protected function locale() {
		return i18n::get_locale();
	}
	
	function Type() {
		return 'countrydropdown dropdown';
	}
}
?>