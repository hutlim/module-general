<?php
/**
 * GridField component for attached models in bulk action
 *
 * @package general
 */
class GridFieldBulkAction implements GridField_HTMLProvider, GridField_ColumnProvider, GridField_URLHandler {
    protected $actions = array();

    function addBulkAction($name, $label = null, $handler = null, $config = null) {
        if(array_key_exists($name, $this->actions)) {
            user_error("Bulk action '$name' already exists.", E_USER_ERROR);
        }

        if(!$label) {
            $label = ucfirst($name);
        }

        if(!$handler) {
            $handler = 'GridFieldBulk' . ucfirst($name) . 'Handler';
        }

        if(!ClassInfo::exists($handler)) {
            user_error("Bulk handler for $name not found: $handler", E_USER_ERROR);
        }

        if($config && !is_array($config)) {
            user_error("Bulk action config should be an array of key => value pairs.", E_USER_ERROR);
        } else {
            $config = array(
                'isAjax' => isset($config['isAjax']) ? $config['isAjax'] : true,
                'icon' => isset($config['icon']) ? $config['icon'] : 'accept'
            );
        }

        $this->actions[$name] = array(
            'label' => $label,
            'handler' => $handler,
            'config' => $config
        );

        return $this;
    }

    function removeBulkAction($name) {
        if(!array_key_exists($name, $this->actions)) {
            user_error("Bulk action '$name' doesn't exists.", E_USER_ERROR);
        }

        unset($this->actions[$name]);

        return $this;
    }

    function augmentColumns($gridField, &$columns) {
        if(!in_array('action_BulkSelect', $columns))
            array_unshift($columns, 'action_BulkSelect');
    }

    function getColumnsHandled($gridField) {
        return array('action_BulkSelect');
    }

    function getColumnContent($gridField, $record, $columnName) {
        $cb = CheckboxField::create('bulk_item_' . $record->ID)->addExtraClass('bulk-item')->addExtraClass('no-change-track')->setAttribute('data-record', $record->ID);
        return $cb->Field();
    }

    function getColumnAttributes($gridField, $record, $columnName) {
        return array('class' => 'col-bulk-item');
    }

    function getColumnMetadata($gridField, $columnName) {
        if($columnName == 'action_BulkSelect') {
            return array('title' => _t('GridFieldBulkAction.SELECT', 'Select'));
        }
    }

    function getHTMLFragments($gridField) {
    	Requirements::javascript(THIRDPARTY_DIR . '/jquery/jquery.js');
		Requirements::javascript(FRAMEWORK_DIR . '/javascript/i18n.js');
		Requirements::add_i18n_javascript('general/javascript/lang');
        Requirements::javascript('general/javascript/GridFieldBulkAction.min.js');
		Requirements::css('general/css/GridFieldBulkAction.css');

        if(!count($this->actions)) {
            user_error("Trying to use GridFieldBulkAction without any bulk action.", E_USER_ERROR);
        }

        $actionsListSource = array();
        $actionsConfig = array();

        foreach($this->actions as $action => $actionData) {
            $actionsListSource[$action] = $actionData['label'];
            $actionsConfig[$action] = $actionData['config'];
        }

        reset($this->actions);
        $firstAction = key($this->actions);

        $dropDownActionsList = DropdownField::create('BulkAction', '')->setSource($actionsListSource)->setAttribute('class', 'no-change-track');

        $templateData = array(
            'Menu' => $dropDownActionsList->FieldHolder(),
            'Button' => array(
                'Label' => _t('GridFieldBulkAction.GO', 'Go'),
                'DataURL' => $gridField->Link('bulkAction'),
                'Icon' => $this->actions[$firstAction]['config']['icon'],
                'DataConfig' => htmlspecialchars(json_encode($actionsConfig), ENT_QUOTES, 'UTF-8')
            ),
            'Select' => array('Label' => _t('GridFieldBulkAction.SELECT_ALL', 'Select All')),
            'Colspan' => (count($gridField->getColumns()) - 1)
        );

        $templateData = new ArrayData($templateData);

        return array('header' => $templateData->renderWith('BulkActionButtons'));
    }

    function getURLHandlers($gridField) {
        return array('bulkAction' => 'handleBulkAction');
    }

    function handleBulkAction($gridField, $request) {
        $controller = $gridField->getForm()->Controller();

        foreach($this->actions as $name => $data) {
            $handlerClass = $data['handler'];
            $urlHandlers = Config::inst()->get($handlerClass, 'url_handlers', Config::UNINHERITED);

            if($urlHandlers)
                foreach($urlHandlers as $rule => $action) {
                    if($request->match($rule, false)) {
                        $handler = Injector::inst()->create($handlerClass, $gridField, $this, $controller);
                        return $handler->handleRequest($request, DataModel::inst());
                    }
                }
        }

        user_error("Unable to find matching bulk handler for " . $request->remaining() . '.', E_USER_ERROR);
    }

}
