<?php
/**
 * Bulk action handler for suspend/activate member
 * 
 * @package general
 */
class GridFieldBulkMemberHandler extends GridFieldBulkHandler {	
	/**
	 * RequestHandler allowed actions
	 * @var array
	 */
	private static $allowed_actions = array('active', 'suspend');


	/**
	 * RequestHandler url => action map
	 * @var array
	 */
	private static $url_handlers = array(
		'active' => 'active',
		'suspend' => 'suspend'
	);
	

	/**
	 * Active the selected records passed from the active bulk action
	 * 
	 * @param SS_HTTPRequest $request
	 * @return SS_HTTPResponse List of activated records ID
	 */
	public function active(SS_HTTPRequest $request){
		$ids = array();
		
		try {
            DB::getConn()->transactionStart();
			foreach ($this->getRecords() as $record){
				if($record->canEdit() && $record->Status == 'Suspend'){
					array_push($ids, $record->ID);
					$record->Status = 'Active';
	            	$record->write();
				}
			}
            DB::getConn()->transactionEnd();
        }
        catch(ValidationException $e){
            DB::getConn()->transactionRollback();
            throw new ValidationException($e->getMessage(), 0);
        }

		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'result' => 'success',
			'message' => _t('GridFieldBulkMemberHandler.SUCCESS_ACTIVATED', 'Total {count} member has been activated', '', array('count' => sizeof($ids)))
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
	
	/**
	 * Suspend the selected records passed from the suspend bulk action
	 * 
	 * @param SS_HTTPRequest $request
	 * @return SS_HTTPResponse List of suspended records ID
	 */
	public function suspend(SS_HTTPRequest $request){
		$ids = array();
		
		try {
            DB::getConn()->transactionStart();
			foreach ($this->getRecords() as $record){
				if($record->canEdit() && $record->Status == 'Active'){
					array_push($ids, $record->ID);
					$record->Status = 'Suspend';
	            	$record->write();
				}
			}
            DB::getConn()->transactionEnd();
        }
        catch(ValidationException $e){
            DB::getConn()->transactionRollback();
            throw new ValidationException($e->getMessage(), 0);
        }

		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'result' => 'success',
			'message' => _t('GridFieldBulkMemberHandler.SUCCESS_SUSPENDED', 'Total {count} member has been suspended', '', array('count' => sizeof($ids)))
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
}