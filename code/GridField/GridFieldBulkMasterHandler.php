<?php
/**
 * Bulk action handler for delete master item
 * 
 * @package general
 */
class GridFieldBulkMasterHandler extends GridFieldBulkHandler
{	
	/**
	 * RequestHandler allowed actions
	 * @var array
	 */
	private static $allowed_actions = array('delete');


	/**
	 * RequestHandler url => action map
	 * @var array
	 */
	private static $url_handlers = array(
		'delete' => 'delete'
	);
	
	/**
	 * Delete the selected records passed from the delete bulk action
	 * 
	 * @param SS_HTTPRequest $request
	 * @return SS_HTTPResponse List of deleted records ID
	 */
	public function delete(SS_HTTPRequest $request){
		$ids = array();
		
		try {
            DB::getConn()->transactionStart();
			foreach ($this->getRecords() as $record){
				if($record->canDelete()){
	            	array_push($ids, $record->ID);
					$record->delete();
				}
			}
            DB::getConn()->transactionEnd();
        }
        catch(ValidationException $e){
            DB::getConn()->transactionRollback();
            throw new ValidationException($e->getMessage(), 0);
        }

		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'result' => 'success',
			'message' => _t('GridFieldBulkMasterHandler.SUCCESS_DELETED', 'Total {count} item has been deleted', '', array('count' => sizeof($ids)))
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
}