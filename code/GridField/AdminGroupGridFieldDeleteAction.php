<?php
class AdminGroupGridFieldDeleteAction extends GridFieldDeleteAction {
	/**
	 *
	 * @param GridField $gridField
	 * @param DataObject $record
	 * @param string $columnName
	 * @return string - the HTML for the column 
	 */
	public function getColumnContent($gridField, $record, $columnName) {
		if($this->removeRelation) {
			if(!Permission::check('EDIT_GROUPS')) return;

			$field = GridField_FormAction::create($gridField, 'UnlinkRelation'.$record->ID, false,
					"unlinkrelation", array('RecordID' => $record->ID))
				->addExtraClass('gridfield-button-unlink')
				->setAttribute('title', _t('GridAction.UnlinkRelation'))
				->setAttribute('data-icon', 'chain--minus');
		} else {
			if(!Permission::check('EDIT_GROUPS')) return;
			
			$field = GridField_FormAction::create($gridField,  'DeleteRecord'.$record->ID, false, "deleterecord",
					array('RecordID' => $record->ID))
				->addExtraClass('gridfield-button-delete')
				->setAttribute('title', _t('GridAction.Delete'))
				->setAttribute('data-icon', 'cross-circle')
				->setDescription(_t('GridAction.DELETE_DESCRIPTION'));
		}
		return $field->Field();
	}
	
	/**
	 * Handle the actions and apply any changes to the GridField
	 *
	 * @param GridField $gridField
	 * @param string $actionName
	 * @param mixed $arguments
	 * @param array $data - form data
	 * @return void
	 */
	public function handleAction(GridField $gridField, $actionName, $arguments, $data) {
		if($actionName == 'deleterecord' || $actionName == 'unlinkrelation') {
			$item = $gridField->getList()->byID($arguments['RecordID']);
			if(!$item) {
				return;
			}
			
			if($actionName == 'deleterecord') {
				if(!Permission::check('EDIT_GROUPS')) {
					throw new ValidationException(
						_t('GridFieldAction_Delete.DeletePermissionsFailure'),0);
				}

				$item->delete();
			} else {
				if(!Permission::check('EDIT_GROUPS')) {
				throw new ValidationException(
					_t('GridFieldAction_Delete.EditPermissionsFailure'),0);
			}

				$gridField->getList()->remove($item);
			}
		} 
	}
}
