<?php
/**
 * @package general
 */
class GridFieldEmailButton implements GridField_ColumnProvider, GridField_ActionProvider {
	protected $email_field = null;
	
	public function __construct($email_field = 'Email') {
        $this->setEmailField($email_field);
    }
	
	public function setEmailField($fieldName){
        $this->email_field = $fieldName;
        return $this;
    }
    
    public function getEmailField(){
        return $this->email_field;
    }
	
    public function augmentColumns($gridField, &$columns) {
        if(!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }

    public function getColumnAttributes($gridField, $record, $columnName) {
        return array('class' => 'col-buttons');
    }

    public function getColumnMetadata($gridField, $columnName) {
        if($columnName == 'Actions') {
            return array('title' => '');
        }
    }

    public function getColumnsHandled($gridField) {
        return array('Actions');
    }

    public function getColumnContent($gridField, $record, $columnName) {
    	$email = $this->getEmailField() ? $record->getField($this->getEmailField()) : '';
		if(Email::is_valid_address($email)) {
			Requirements::javascript(THIRDPARTY_DIR . '/jquery/jquery.js');
			Requirements::javascript(FRAMEWORK_DIR . '/javascript/i18n.js');
			Requirements::add_i18n_javascript('general/javascript/lang');
		    Requirements::javascript('general/javascript/GridFieldEmailButton.min.js');
		    Requirements::css('general/css/GridFieldEmailButton.css');

			return GridField_FormAction::create($gridField, 'Email' . $record->ID, false, "email", array('RecordID' => $record->ID))->addExtraClass('gridfield-button-email')->setAttribute('title', _t('GridFieldEmailButton.BUTTONSEND', 'Send'))->setAttribute('data-icon', 'email')->setAttribute('data-email', $email)->setDescription(_t('GridFieldEmailButton.SEND_EMAIL', 'Send Email'))->Field();
		}
    }

    public function getActions($gridField) {
        return array(
            'email'
        );
    }

    public function handleAction(GridField $gridField, $actionName, $arguments, $data) {
        if($actionName == 'email') {
            $item = $gridField->getList()->byID($arguments['RecordID']);
            if(!$item) {
                return;
            }
			
            try {
	            $e = Email::create();
	            $e->setSubject($data['Subject']);
	            $e->setBody($data['Message']);
	            $e->setTo($item->getField($this->getEmailField()));
	            if(!$e->send()){
	                throw new ValidationException(_t('GridFieldEmailButton.ERROR_SEND_EMAIL', 'Error occur while sending email'), 0);
	            }

				$item->IsSent = 1;
				$item->write();
	        }
	        catch(ValidationException $e) {
	            DB::getConn()->transactionRollback();
	            throw new ValidationException($e->getMessage(), 0);
	        }
        }
    }
}
