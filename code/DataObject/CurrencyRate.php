<?php

class CurrencyRate extends DataObject implements PermissionProvider {
    private static $singular_name = "Currency Rate";
    private static $plural_name = "Currency Rates";

    private static $db = array(
        'FromCurrency' => 'Varchar',
        'ToCurrency' => 'Varchar',
        'Rate' => 'Decimal(15,6)',
        'Country' => 'Varchar(2)'
    );
	
	private static $has_one = array(
		'Admin' => 'Member'
	);
	
	private static $default_sort = "LastEdited DESC";

    private static $searchable_fields = array(
    	'LastEdited' => array(
			'field' => 'DateField',
			'filter' => 'DateMatchFilter'
		),
		'Admin.Username',
        'FromCurrency',
        'ToCurrency',
        'Rate',
        'Country' => array(
			'field' => 'CountryDropdownField'
		)
    );

    private static $summary_fields = array(
    	'LastEdited.Nice',
        'FromCurrency',
        'ToCurrency',
        'Rate.Nice',
        'CountryTitle',
        'Admin.Username'
    );

    function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

		$labels['LastEdited'] = _t('CurrencyRate.UPDATED_ON', 'Updated On');
        $labels['LastEdited.Nice'] = _t('CurrencyRate.UPDATED_ON', 'Updated On');
        $labels['FromCurrency'] = _t('CurrencyRate.FROM_CURRENCY', 'From Currency');
		$labels['ToCurrency'] = _t('CurrencyRate.TO_CURRENCY', 'To Currency');
		$labels['Rate'] = _t('CurrencyRate.RATE', 'Rate');
        $labels['Rate.Nice'] = _t('CurrencyRate.RATE', 'Rate');
		$labels['Country'] = _t('CurrencyRate.COUNTRY', 'Country');
		$labels['CountryTitle'] = _t('CurrencyRate.COUNTRY', 'Country');
		$labels['Admin.Username'] = _t('CurrencyRate.UPDATED_BY', 'Updated By');
        return $labels;
    }
	
	function populateDefaults() {
		parent::populateDefaults();
		$this->FromCurrency = SiteCurrencyConfig::current_site_currency();
		return $this;
	}

	function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$source = Zend_Locale::getTranslationList('nametocurrency', i18n::get_locale());
		if(class_exists('Collator') && ($collator = Collator::create(i18n::get_locale()))) {
			$collator->asort($source);
		}
		else {
			asort($source);
		}
		
		$fields->replaceField('FromCurrency', DropdownField::create('FromCurrency', $this->fieldLabel('FromCurrency'), $source));
		$fields->makeFieldReadonly('FromCurrency');
		$fields->replaceField('ToCurrency', DropdownField::create('ToCurrency', $this->fieldLabel('ToCurrency'), $source));
		$fields->replaceField('Country', CountryDropdownField::create('Country', $this->fieldLabel('Country')));
		$fields->removeByName('AdminID');
		return $fields;
	}
	
	function onBeforeWrite(){
		parent::onBeforeWrite();
		$this->AdminID = Member::currentUserID();
	}
	
	function getTitle(){
		return sprintf('%s/%s (%s)', $this->FromCurrency, $this->ToCurrency, $this->dbObject('Rate')->Nice());
	}
	
	function getFromCurrencyName(){
		require_once 'Zend/Currency.php';
		$currency = new Zend_Currency(i18n::get_locale());
		return $currency->getName($this->FromCurrency, i18n::get_locale());
	}
	
	function getToCurrencyName(){
		require_once 'Zend/Currency.php';
		$currency = new Zend_Currency(i18n::get_locale());
		return $currency->getName($this->ToCurrency, i18n::get_locale());
	}

	function getCountryTitle() {
        return Zend_Locale::getTranslation($this->Country, "country", i18n::get_locale());
    }

    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_CurrencyRate');
    }

    function canEdit($member = false) {
    	if(!$this->exists() && $this->canCreate($member)){
    		return true;
    	}
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('EDIT_CurrencyRate');
    }

    function canDelete($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('DELETE_CurrencyRate');
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('CREATE_CurrencyRate');
    }

    public function providePermissions() {
        return array(
            'VIEW_CurrencyRate' => array(
                'name' => _t('CurrencyRate.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('CurrencyRate.PERMISSIONS_CATEGORY', 'Currency Rate')
            ),
            'EDIT_CurrencyRate' => array(
                'name' => _t('CurrencyRate.PERMISSION_EDIT', 'Allow edit access right'),
                'category' => _t('CurrencyRate.PERMISSIONS_CATEGORY', 'Currency Rate')
            ),
            'DELETE_CurrencyRate' => array(
                'name' => _t('CurrencyRate.PERMISSION_DELETE', 'Allow delete access right'),
                'category' => _t('CurrencyRate.PERMISSIONS_CATEGORY', 'Currency Rate')
            ),
            'CREATE_CurrencyRate' => array(
                'name' => _t('CurrencyRate.PERMISSION_CREATE', 'Allow create access right'),
                'category' => _t('CurrencyRate.PERMISSIONS_CATEGORY', 'Currency Rate')
            )
        );
    }

}
?>