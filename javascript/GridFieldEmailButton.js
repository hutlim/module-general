(function($){
	$.entwine('ss', function($) {
		$('.ss-gridfield .col-buttons .action.gridfield-button-email').entwine({
			onclick: function(e) {
				var self = this;
				$('#modal').remove();
				var content = '<div id="modal"><div class="field text"><label class="left" for="Subject">' + ss.i18n._t('GridFieldEmailButton.SUBJECT', 'Subject') + '</label><input id="Subject" class="text" type="text" /></div><div class="field textarea"><label class="left" for="Message">' + ss.i18n._t('GridFieldEmailButton.MESSAGE', 'Message') + '</label><textarea rows="5" cols="50" id="Message" class="textarea"></textarea></div></div>';
				var actions = {};
				actions[ss.i18n._t('GridFieldEmailButton.SEND', 'Send')] = function() {
					var subject = $('#modal').find('#Subject').val(), message = $('#modal').find('#Message').val();
	        		
	        		if(subject == ''){
	        			alert(ss.i18n._t('GridFieldEmailButton.SUBJECT_REQUIRED', 'Subject is required'));
	        			return false;
	        		}
	        		else if(message == ''){
	        			alert(ss.i18n._t('GridFieldEmailButton.MESSAGE_REQUIRED', 'Message is required'));
	        			return false;
	        		}

	          		$(this).dialog("close");

					// If the button is disabled, do nothing.
					if (self.button('option', 'disabled')) {
						e.preventDefault();
						return;
					}

					self.getGridField().reload(
						{data: [{name: self.attr('name'), value: self.val()}, {name: 'Subject', value: subject}, {name: 'Message', value: message}]},
						function(){
							statusMessage(ss.i18n._t('GridFieldEmailButton.EMAIL_SENT', 'Email has been sent successfully'), 'good');
						}
					);
					
					e.preventDefault();
	        	};
				$('body').append(content);
				$('#modal').dialog({
					resizable : false,
					modal : true,
					autoOpen : true,
					width: 500,
			      	buttons: actions
				});
				
				return false;
			}
		});
	});
}(jQuery));
