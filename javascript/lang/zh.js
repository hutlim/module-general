if ( typeof (ss) == 'undefined' || typeof (ss.i18n) == 'undefined') {
	console.error('Class ss.i18n not defined');
} else {
	ss.i18n.addDictionary('zh', {
		'MemberGridFieldStatusAction.ACTIVE_MEMBER_CONFIRMATION' : '你确定要激活此会员？',
		'MemberGridFieldStatusAction.SUSPEND_MEMBER_CONFIRMATION' : '你确定要中止此会员？',
		'CurrencyDropdownField.ERROR_LOADING' : '加载时发生错误，请重试。',
		'GeneralModalAdmin.CREATE_CONFIRMATION' : '你确定你要创建此项目？',
		'GeneralModalAdmin.EDIT_CONFIRMATION' : '你确定你要保存此项目？',
		'GridFieldBulkAction.EMPTY_SELECT' : '你必须至少选择一个项目。',
		'GridFieldBulkAction.CONFIRM_ACTION' : '你确定要执行此动作？',
		'GridFieldBulkAction.ERROR_LOADING' : '加载时发生错误，请重试。',
		'GridFieldEmailButton.SUBJECT' : '标题',
		'GridFieldEmailButton.MESSAGE' : '信息',
		'GridFieldEmailButton.SEND' : '发送',
		'GridFieldEmailButton.SUBJECT_REQUIRED' : '标题为必填',
		'GridFieldEmailButton.MESSAGE_REQUIRED' : '信息为必填',
		'GridFieldEmailButton.EMAIL_SENT' : '信息已发送成功',
		'GeneralLeftAndMain.ERROR_NETWORK' : '连接已经被意外关闭，请检查你的网络连接，然后重试。',
		'UniqueUsernameField.ERROR_LOADING' : '加载时发生错误，请重试。',
		'General.LOADING' : '载入中...'
	});
}