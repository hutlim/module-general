if ( typeof (ss) == 'undefined' || typeof (ss.i18n) == 'undefined') {
	console.error('Class ss.i18n not defined');
} else {
	ss.i18n.addDictionary('en', {
		'MemberGridFieldStatusAction.ACTIVE_MEMBER_CONFIRMATION' : 'Are you sure you want to active this member?',
		'MemberGridFieldStatusAction.SUSPEND_MEMBER_CONFIRMATION' : 'Are you sure you want to suspend this member?',
		'CurrencyDropdownField.ERROR_LOADING' : 'Error occur while loading, please try again.',
		'GeneralModalAdmin.CREATE_CONFIRMATION' : 'Are you sure you want to create these item?',
		'GeneralModalAdmin.EDIT_CONFIRMATION' : 'Are you sure you want to save these item?',
		'GridFieldBulkAction.EMPTY_SELECT' : 'You must select at least one item.',
		'GridFieldBulkAction.CONFIRM_ACTION' : 'Are you sure want to perform these action?',
		'GridFieldBulkAction.ERROR_LOADING' : 'Error occur while loading, please try again.',
		'GridFieldEmailButton.SUBJECT' : 'Subject',
		'GridFieldEmailButton.MESSAGE' : 'Message',
		'GridFieldEmailButton.SEND' : 'Send',
		'GridFieldEmailButton.SUBJECT_REQUIRED' : 'Subject is required',
		'GridFieldEmailButton.MESSAGE_REQUIRED' : 'Message is required',
		'GridFieldEmailButton.EMAIL_SENT' : 'Email has been sent successfully',
		'GeneralLeftAndMain.ERROR_NETWORK' : 'Connection has been unexpectedly closed, please check you internet and try again.',
		'UniqueUsernameField.ERROR_LOADING' : 'Error occur while loading, please try again.',
		'General.LOADING' : 'Loading...'
	});
}