(function($){
	/*"use strict";*/
	$.entwine('ss', function($) {
		$('.cms-content-tools #Form_SearchForm').entwine({
			onsubmit: function(e) {
				// Remove empty elements and make the URL prettier
				var nonEmptyInputs,
					url;

				nonEmptyInputs = this.find(':input:not(:submit)').filter(function() {
					// Use fieldValue() from jQuery.form plugin rather than jQuery.val(),
					// as it handles checkbox values more consistently
					var vals = $.grep($(this).fieldValue(), function(val) { return (val);});
					return (vals.length);
				});

				url = this.attr('action');

				if(nonEmptyInputs.length) {
					url = $.path.addSearchParams(url, nonEmptyInputs.serialize());
				}

				this.closest('.cms-container').loadPanel(url, "", {}, true);

				return false;
			}
		});
		
		$('.importSpec').entwine({
			onmatch: function() {
				this.find('div.details').hide();
				this.find('a.detailsLink').click(function() {
					$('#' + $(this).attr('href').replace(/.*#/,'')).slideToggle();
					return false;
				});
				
				this._super();
			},
			onunmatch: function() {
				this._super();
			}
		});

		$('.cms-edit-form .Actions input.action[type=submit], .cms-edit-form .Actions button.action').entwine({
			onclick: function(e) {
				var msg = this.data('icon') == 'add' ? ss.i18n._t('GeneralModalAdmin.CREATE_CONFIRMATION', 'Are you sure you want to create these item?') : ss.i18n._t('GeneralModalAdmin.EDIT_CONFIRMATION', 'Are you sure you want to save these item?');
				if (this.attr('name') == 'action_doSave' && !confirm(msg)) {
					e.preventDefault();
					return false;
				} else {
					this._super(e);
				}
			}
		});
	});

}(jQuery));