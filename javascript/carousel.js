(function($) {
	$('.carousel .carousel-control').hide();

	$('.carousel').carousel({
		interval : 3000
	});

	$('.item').find('img').bind("load", function() {
		$(this).fadeIn('slow');
	});
	pre_load();
	
	$('.carousel').on('slid.bs.carousel', function(e) {
		$('.carousel .carousel-control').show();
		pre_load();
	});
	
	function pre_load(){
		var url = $('.item.active').data('url');
		$('.item.active').find('img').attr("src", url);
	}
})(jQuery);
