(function($){
	/*"use strict";*/
	$.entwine('ss', function($) {
		$('.ss-gridfield .col-buttons .action.gridfield-button-active').entwine({
			onclick: function(e){
				if(confirm(ss.i18n._t('MemberGridFieldStatusAction.ACTIVE_MEMBER_CONFIRMATION', 'Are you sure you want to active this member?'))) {
					this._super(e);
				} else {
					e.preventDefault();
					return false;
				}
			}
		});
		
		$('.ss-gridfield .col-buttons .action.gridfield-button-suspend').entwine({
			onclick: function(e){
				if(confirm(ss.i18n._t('MemberGridFieldStatusAction.SUSPEND_MEMBER_CONFIRMATION', 'Are you sure you want to suspend this member?'))) {
					this._super(e);
				} else {
					e.preventDefault();
					return false;
				}
			}
		});
	});

}(jQuery));