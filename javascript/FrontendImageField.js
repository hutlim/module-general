(function($) {
	$('div.fileupload a.upload-action').live('click', function(e) {
		e.preventDefault();
	
		var node = $(this);
		var url = node.attr('href');
		var options = { 
	        url: url,
	        type: 'post',
	        dataType: 'json',
	        success: function(result){
	        	if(result.error != '' && result.error != undefined){
	        		alert(result.error);
	        		return false;
	        	}
	        	
				if(result.content != '' && result.content != undefined){
	        		node.closest('div.field.frontendimage').replaceWith(result.content);
	        	}
	        }
	    };
		$(this).parents('form').ajaxSubmit(options);
	});
	
	$('div.fileupload a.delete-action').live('click', function(e) {
		e.preventDefault();
	
		var node = $(this);
		var url = node.attr('href');
		$.get(url, function(data) {
  			node.parents('div.field.frontendimage').replaceWith(data);
		});
	});
}(jQuery));