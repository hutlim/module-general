( function($) {
	$.entwine('ss', function($) {
		$('td.col-bulk-item').entwine({
			onmouseover: function(){
				$(this).parents('.ss-gridfield-item').find('.edit-link').removeClass('edit-link').addClass('hide-edit-link');
			},
			onmouseout: function(){
				$(this).parents('.ss-gridfield-item').find('.hide-edit-link').addClass('edit-link').removeClass('hide-edit-link');
			},
			onclick : function(e) {
				var cb = $(e.target).find('input:checkbox'), parent = $(this).parents('.ss-gridfield-table');
				if (!$(cb).is(':checked')){
					$(cb).prop('checked', true).trigger('change');
				}
				else{
					$(cb).prop('checked', false).trigger('change');
				}
			}
		});
		
		$('td.col-bulk-item input:checkbox').entwine({
			onchange : function(e) {
				var parent = $(this).parents('.ss-gridfield-table');
				if ($(this).is(':checked')){
					var flag = 0;
		            parent.find('td.col-bulk-item input:checkbox').each(function(){
		                if(!$(this).is(':checked')){
		                	flag = 1;
		                }
		            }); 
                    
                    if(flag == 0){ 
                    	parent.find('input.bulk-all').prop("checked", true);
                    }
				}
				else{
					parent.find('input.bulk-all').prop('checked', false);
				}
				return false;
			}
		});

		$('input:checkbox.bulk-all').entwine({
			onclick : function() {
				var state = $(this).is(':checked');
				$(this).parents('.ss-gridfield-table').find('td.col-bulk-item input:checkbox').prop('checked', state).trigger('change');
			},
			getSelectRecordsID : function() {
				return $(this).parents('.ss-gridfield-table').find('td.col-bulk-item input:checked').map(function() {
					return parseInt($(this).data('record'));
				}).get();
			}
		});

		$("select[name='BulkAction']").entwine({
			onchange : function(e) {
				var value = $(this).val(), parent = $(this).parents('.bulk-button'), btn = parent.find('.action'), config = btn.data('config'), icon = parent.find('.action .ui-icon');

				$.each(config, function(key, data) {
					if (key != value) {
						icon.removeClass('btn-icon-' + data['icon']);
					}
				});
				
				icon.addClass('btn-icon-' + config[value]['icon']);
				
				return false;
			}
		});

		$('.bulk-button .action').entwine({
			getActionURL : function(action, url) {
				var cacheBuster = new Date().getTime();
				url = url.split('?');

				if (action) {
					action = '/' + action;
				} else {
					action = '';
				}

				if (url[1]) {
					url = url[0] + action + '?' + url[1] + '&' + 'cacheBuster=' + cacheBuster;
				} else {
					url = url[0] + action + '?' + 'cacheBuster=' + cacheBuster;
				}
				return url;
			},
			onclick : function(e) {
				var parent = $(this).parents('.bulk-action'), action = parent.find("select[name='BulkAction']").val(), ids = parent.find('input:checkbox.bulk-all:first').getSelectRecordsID();
				this.doBulkAction(action, ids);
				return false;
			},
			doBulkAction : function(action, ids) {
				var self = $(this), parent = $(this).parents('.bulk-action'), btn = parent.find('.bulk-button .action'), config = btn.data('config'), url = this.getActionURL(action, $(this).data('url')), data = { records : ids };

				if (ids.length <= 0) {
					alert(ss.i18n._t('GridFieldBulkAction.EMPTY_SELECT', 'You must select at least one item.'));
					return false;
				}

				if (!confirm(ss.i18n._t('GridFieldBulkAction.CONFIRM_ACTION', 'Are you sure want to perform these action?'))) {
					return false;
				}

				btn.addClass('loading');

				if (config[action]['isAjax']) {
					$.post(url, data, 'json')
					.always(function() {
				    	btn.removeClass('loading');
			  		})
					.done(function(data) {
						if(data.message != '' && data.message != undefined){
							statusMessage(data.message, (data.result == 'success') ? 'good' : 'bad');
						}
						
						if(data.result == 'success'){
							self.parents('.ss-gridfield').reload();
						}
					})
					.fail(function(jqxhr, textStatus, error) {
						errorMessage(ss.i18n._t('GridFieldBulkAction.ERROR_LOADING', 'Error occur while loading, please try again.'));
					});
				} else {
					var records = 'records[]=' + ids.join('&records[]=');
					url = url + '&' + records;

					window.location.href = url;
				}
			}
		});
	});
}(jQuery)); 