(function($) {
	$(":text[rel~=daterange]").livequery(function() {
		if($(this).data('datepicker')) {

		} else {
			var config = $.extend($(this).data(), $(this).data('jqueryuiconfig'), {});
			
			if(config.locale && $.datepicker.regional[config.locale]) {
				config = $.extend(config, $.datepicker.regional[config.locale], {});
			}
			
			config.dateFormat = config.jquerydateformat;
			$(this).daterange(config);
		}
	});
})(jQuery);
