(function($) {
	function addHashToUrl($url) {
		if ('' == $url || undefined == $url) {
			$url = '_';
			// it is empty hash because if put empty string here then browser will scroll to
			// top of page
		}
		$hash = $url.replace(/^.*#/, '');
		var $fx, $node = jQuery('#' + $hash);
		if ($node.length) {
			$fx = jQuery('<div></div>').css({
				position : 'absolute',
				visibility : 'hidden',
				top : jQuery(window).scrollTop() + 'px'
			}).attr('id', $hash).appendTo(document.body);
			$node.attr('id', '');
		}
		document.location.hash = $hash;
		if ($node.length) {
			$fx.remove();
			$node.attr('id', $hash);
		}
	}
	
// ajax loading
	if ($("#spinner").length) {
		$("#spinner").remove();
	}

	var styles = {
		'position' : "fixed",
		'top' : "50%",
		'left' : "50%",
		'margin-left' : '-50px',
		'margin-top' : '-50px',
		'text-align' : 'center',
		'z-index' : '99999',
		'overflow' : 'auto',
		'width' : '100px',
		'height' : '102px',
		'display' : 'none'
	};

	$('body').append($("<div>", {
		id : "spinner",
		css : styles
	}).append($("<img />", {
		id : "img-spinner",
		src : "general/images/spinner.gif"
	})));

	$.ajaxSetup({
  		spinner: true
	});

	$(document).ajaxSend(function(event, request, settings){
		if(settings.spinner === true){
			$('#spinner').show();
		}
	})
	.ajaxComplete(function() {
  		$('#spinner').hide();
	});
// ajax loading

// button link
	$("button.action").live('click', function(e) {
		e.preventDefault();
		var link = $(this).data('link');
		if (link != '' && link != undefined) {
			window.location.href = link;
		}
	});
// button link

// popover
	$('[rel~=popover]').popover({
		placement : 'auto',
		trigger : 'hover'
	});
	$(document).bind("ajaxComplete", function() {
		$('[rel~=popover]').popover({
			placement : 'auto',
			trigger : 'hover'
		});
	});
// popover

// tooltip
	if($.browser.mobile == false){
		$('[rel~=tooltip]').tooltip({
			trigger : 'hover'
		});
		$(document).bind("ajaxComplete", function() {
			$('[rel~=tooltip]').tooltip({
				trigger : 'hover'
			});
		});
	}
// tooltip

// tab
	var hash = document.location.hash;
	if (hash) {
		$('.nav-tabs a[href=' + hash + ']').tab('show');
	}

	// Change hash for page-reload
	$('.nav-tabs a').on('shown.bs.tab', function(e) {
		addHashToUrl(e.target.hash);
	});
// tab

// collapse
	var hash = document.location.hash;
	if (hash) {
		$('.panel-group div[id=' + hash + ']').collapse('show');
	}

	// Change hash for page-reload
	$('.collapse').on('shown.bs.collapse', function(e) {
		addHashToUrl(this.id);
	});
// collapse

// carousel
	$('.carousel').carousel({
		interval : 3000
	});
// carousel

// popup
	// start modal div
	var content = '<div id="popup_modal" class="modal">';

	// start dialog
	content += '<div class="modal-dialog">';

	// start content
	content += '<div class="modal-content">';

	// header
	content += '<div class="modal-header">';
    content += '<h4 class="modal-title" style="float: left;">&nbsp</h4>';
	content += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
	content += '</div>';

	// body
	content += '<div class="modal-body"></div>';

	// footer
	content += '<div class="modal-footer"></div>';

	// close content
	content += '</div>';

	// close dialog
	content += '</div>';

	// close modal div
	content += '</div>';
	$('body').append(content);

	var $popup_modal = $('#popup_modal'), $popup_modal_title = $popup_modal.find('.modal-title'), $popup_modal_body = $popup_modal.find('.modal-body');

	$popup_modal.on('shown.bs.modal', function() {
		$popup_modal_body.find('form').ajaxForm({
			delegation : true,
			target : '#popup_modal .modal-body',
			'beforeSubmit' : function(arr, form, options) {
				$('.holder-required').removeClass('holder-required');
				$('span.message').remove();
			}
		});
	});

	$popup_modal.on('hide.bs.modal', function() {
		$popup_modal_title.html();
		$popup_modal_body.html();
	});

	$('[rel~=popup]').live('click', function(e) {
		e.preventDefault();
		var url = this.href, title = $(this).data('title'), type = $(this).data('type'), pass_value = $(this).data('value');
		if (pass_value == undefined || pass_value == '') {
			pass_value = {};
		}

		if ($(this).data('close-reload') == true) {
			$popup_modal.on('hide.bs.modal', function() {
				document.location.reload(true);
			});
		}

		if ($(this).data('modal-lg') == true) {
			$popup_modal.find('.modal-dialog').addClass('modal-lg');
			$popup_modal.find('.modal-dialog').removeClass('modal-sm');
		}
		else if ($(this).data('modal-sm') == true) {
			$popup_modal.find('.modal-dialog').addClass('modal-sm');
			$popup_modal.find('.modal-dialog').removeClass('modal-lg');
		}
		else {
			$popup_modal.find('.modal-dialog').removeClass('modal-sm');
			$popup_modal.find('.modal-dialog').removeClass('modal-lg');
		}

		if (url.indexOf('#') == 0) {
			$(url).modal('show');
		}
		else {
			$popup_modal.on('shown.bs.modal', function() {
				$popup_modal_title.text(title);
			});

			if (type == 'img') {
				$popup_modal_body.html('<img src="' + url + '" />');
				$popup_modal.modal('show');
			}
			else {
				$.get(url, pass_value, function(data) {
					$popup_modal_body.html(data);
					$popup_modal.modal('show');
				}).fail(function(jqXHR, textStatus, errorThrown) {
					$popup_modal_body.html('<p>Error occur while load these page, please try again.</p>');
					$popup_modal.modal('show');
				});
			}
		}
	});
// popup

// print
	$(".print").live('click', function(e) {
		e.preventDefault();
		$("#spinner").show();
		var url = $(this).attr("href");

		// remove old printframe
		$("#printframe").remove();

		// create new printframe
		var $iframe = $('<iframe/>', {
            id: 'printframe',
            src: url,
            style: 'display: none',
            load:function(e){
            	$("#spinner").hide();
                // nasty hack to be able to print the frame
				var tempFrame = $('#printframe')[0];
				var tempFrameWindow = tempFrame.contentWindow ? tempFrame.contentWindow : tempFrame.contentDocument.defaultView;
				tempFrameWindow.focus();
				tempFrameWindow.print();
            },
            error:function(e){
            	$("#spinner").hide();
            }
        });

		// load printframe
		if ($iframe != null && url != null) {
			$('body').append($iframe); 
		}
		else {
			$("#spinner").hide();
		}
	});
// print

// input mask
	$(document).ready(function(){
		$(':input').inputmask();
		$('input.numeric').inputmask('decimal', {rightAlign: false, digits: 2, groupSeparator: ',', autoGroup: false});
		$('input.integer').inputmask('integer', {rightAlign: false, groupSeparator: ',', autoGroup: false});
		$('input.currency').inputmask('currency', {rightAlign: false, groupSeparator: ',', autoGroup: false});
		$('input.alphanumeric').inputmask('Regex', {regex: '^[a-zA-Z0-9]+$'});
	});
	$(document).bind("ajaxComplete", function() {
		$(':input').inputmask();
		$('input.numeric').inputmask('decimal', {rightAlign: false, digits: 2, groupSeparator: ',', autoGroup: false});
		$('input.integer').inputmask('integer', {rightAlign: false, groupSeparator: ',', autoGroup: false});
		$('input.currency').inputmask('currency', {rightAlign: false, groupSeparator: ',', autoGroup: false});
		$('input.alphanumeric').inputmask('Regex', {regex: '^[a-zA-Z0-9]+$'});
	});
// input mask

// fix required
	$(document).ready(function(){
		$('input[type=hidden]').removeAttr('required');
		$('.htmleditor').removeAttr('required');
	});
	$(document).bind("ajaxComplete", function() {
		$('input[type=hidden]').removeAttr('required');
		$('.htmleditor').removeAttr('required');
	});
// fix required

// submit form disable button
	$("form").submit(function(event) {
  		$(this).find(':submit').data('loading-text', ss.i18n._t('General.LOADING', 'Loading...')).button('loading');
	});
//
})(jQuery);
