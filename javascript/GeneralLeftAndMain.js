(function($) {
	/*"use strict";*/
	$.entwine('ss', function($) {
		$(document).ajaxError(function(e, xhr, settings) {
			if (xhr.status === 0 && settings.check_network !== true) {
				$.ajax({
					url : 'Security/ping',
					cache : false,
					type : "HEAD",
					check_network : true
				})
				.fail(function() {
					statusMessage(decodeURIComponent(ss.i18n._t('GeneralLeftAndMain.ERROR_NETWORK', 'Connection has been unexpectedly closed, please check you internet and try again.')), 'bad');
				});
			}
		});
	});
}(jQuery));