(function($) {
	$(':text[rel~=currency], select[rel~=currency]').each(function(index) {
		var self = $(this), id = self.attr('id'), container = $(this).parent();
		if(self.data('country-field') != ''){
			var country_field = self.parents('form').find(':input[name="' + self.data('country-field') + '"]'), country = country_field.val(), url = self.data('url'), country_field_id = country_field.attr('id');
			if(country != '' && url != ''){
				$.ajax({
					url: url,
		          	dataType: 'html',
		          	data: {'country': country},
		          	beforeSend: function(xhr) {
		          		self.prop('disabled', true).addClass('loading').trigger('liszt:updated');
		          		self.next('.chzn-container').find('a span').html('Loading...').addClass('loading');
		          		country_field.addClass('load-currency').prop('disabled', true).trigger('liszt:updated');
		          	},
		          	spinner: false
		        })
				.done(function(data) {
					if(data != ''){
						var obj = container.clone();
						obj.find(':text[rel~=currency], select[rel~=currency]').remove();
						var message = obj.html();
						if(message != undefined){
							data += message;
						}
						container.html(data);
					}
					else{
						alert(ss.i18n._t('CurrencyDropdownField.ERROR_LOADING', 'Error occur while loading, please try again.'));
					}
				})
				.always(function() {
					self.prop('disabled', false).removeClass('loading').trigger('liszt:updated');
					country_field.removeClass('load-currency');
					if(country_field.is('[class*=load-]') === false){
						country_field.prop('disabled', false).trigger('liszt:updated');
					}
		  		})
		  		.fail(function() {
    				alert(ss.i18n._t('CurrencyDropdownField.ERROR_LOADING', 'Error occur while loading, please try again.'));
  				});
			}

			$('#' + country_field_id).live("change", function(e) {
				e.preventDefault();
				var self = $(this), country = self.val(), currency_field = $('#' + id), container = currency_field.parent(), url = currency_field.data('url');
				if(country != '' && url != ''){
					$.ajax({
						url: url,
			          	dataType: 'html',
			          	data: {'country': country},
			          	beforeSend: function(xhr) {
			          		currency_field.prop('disabled', true).addClass('loading').trigger('liszt:updated');
			          		currency_field.next('.chzn-container').find('a span').html('Loading...').addClass('loading');
			          		self.addClass('load-currency').prop('disabled', true).trigger('liszt:updated');
			          	},
			          	spinner: false
			        })
					.done(function(data) {
						if(data != ''){
							var obj = container.clone();
							obj.find(':text[rel~=currency], select[rel~=currency]').remove();
							var message = obj.html();
							if(message != undefined){
								data += message;
							}
							container.html(data);
						}
						else{
							alert(ss.i18n._t('CurrencyDropdownField.ERROR_LOADING', 'Error occur while loading, please try again.'));
						}
					})
					.always(function() {
						currency_field.prop('disabled', false).removeClass('loading').trigger('liszt:updated');
						self.removeClass('load-currency');
						if(self.is('[class*=load-]') === false){
							self.prop('disabled', false).trigger('liszt:updated');
						}
			  		})
			  		.fail(function() {
	    				alert(ss.i18n._t('CurrencyDropdownField.ERROR_LOADING', 'Error occur while loading, please try again.'));
	  				});
				}
			});
		}
	});
})(jQuery);