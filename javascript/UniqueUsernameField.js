(function($) {
	$(':text[rel~=uniqueusername]').live("change", function(e) {
		e.preventDefault();
		var field = $(this);
		var form_container = field.parents('form');
		if(field.val() == '' || field.val() == field.data('username')){
			return false;
		}
		
		form_container.find('input[type="submit"], input[type="button"], button').prop('disabled', true);
		field.prop('disabled', true).addClass('loading');
		$.ajax({
			url: $(this).data('url'),
          	dataType: 'json',
          	data: {'username': field.val()},
          	spinner: false
        })
		.always(function() {
			form_container.find('input[type="submit"], input[type="button"], button').prop('disabled', false);
			field.prop('disabled', false).removeClass('loading');
  		})
		.done(function(data) {
		    if(data.result != 'success'){
		    	alert(data.msg);
		    	field.val('');
		    	field.focus();
		    }
		})
		.fail(function( jqxhr, textStatus, error ) {
			alert(ss.i18n._t('UniqueUsernameField.ERROR_LOADING', 'Error occur while loading, please try again.'));
		    field.val('');
		    field.focus();
		});
	});
})(jQuery);
