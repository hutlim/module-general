(function($) {
	$('textarea[rel~=summernote]').summernote({
        height: 400,
  		onImageUpload: function(files, editor, editable) {
            var upload_link = $(editable).parent().parent().children('textarea[rel~=summernote').data('upload-image');
            send_file(files[0], editor, editable, upload_link);
        }
	});
	
	$(document).bind("ajaxComplete", function() {
		$('textarea[rel~=summernote').summernote({
	        height: 400,
	  		onImageUpload: function(files, editor, editable) {
	            var upload_link = $(editable).parent().parent().children('textarea[rel~=summernote').data('upload-image');
	            send_file(files[0], editor, editable, upload_link);
	        }
		});
	});
	
	function send_file(file, editor, editable, upload_link) {
		data = new FormData();
      	data.append("file", file);
      	$.ajax({
          	data: data,
          	type: "post",
          	url: upload_link,
          	cache: false,
          	contentType: false,
          	processData: false,
          	dataType: 'json',
          	success: function(data) {
          		if(data.error){
          			alert(data.error);
          		}
          		else{
            		editor.insertImage(editable, data.path);
            	}
          	}
      	});
	}
}(jQuery));