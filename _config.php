<?php

define('GENERAL_PATH', __DIR__);

Authenticator::register('AdminAuthenticator');
Authenticator::unregister('MemberAuthenticator');
Authenticator::set_default_authenticator('AdminAuthenticator');

CMSMenu::remove_menu_item('SecurityAdmin');
CMSMenu::remove_menu_item('AdminProfileController');